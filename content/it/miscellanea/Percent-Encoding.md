+++
Title = "Codifica URL"
+++
<div class="Percent-Encoding">
<label>Plain</label>
<textarea name="plain"></textarea>
<label>URL-encoded</label>
<textarea name="coded"></textarea>
<p>
<label>Passaggi: <input name="steps" type="number" value="1" min="1" /></label>
</p>
<link rel="stylesheet" href="/res/Percent-Encoding.css" />
<script src="/res/Percent-Encoding.js"></script>
</div>
