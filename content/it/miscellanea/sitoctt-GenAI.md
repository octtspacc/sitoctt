+++
Title = "🧙️ Politica del sitoctt sull'Intelligenza Artificiale Generativa"
Categories = [ "Meta" ]
Lastmod = 2025-01-11
+++

A causa delle controversie concernenti le intelligenze artificiali generative e il loro utilizzo,[^concerns] questa pagina vuole, nel possibile, delineare una politica sul loro utilizzo ed inutilizzo nel contesto di questo sito, per garantire un buon livello di trasparenza con il pubblico.

La data dell'ultimo aggiornamento di questo documento è indicata in cima alla pagina, sotto il titolo.  
Le revisioni passate possono essere consultate dalla cronologia di Git: [sitoctt-GenAI.md](https://gitlab.com/octtspacc/sitoctt/-/commits/sitoctt-next/content/it/miscellanea/sitoctt-GenAI.md).

## Testi

Nessun modello generativo di linguaggio è mai stato usato per nessuna delle tappe del ciclo di vita del contenuto originale di pagine e post.  
Questo include, ma non si limita a: abbozzamento, composizione, revisione, aggiornamenti successivi.

Vengono tuttavia usati strumenti di traduzione di testo proprietari, tra cui Google Traduttore, per tradurre automaticamente lato server le pagine e i post del sito in una moltitudine di diverse lingue.  
Questi sistemi potrebbero o meno essere basati su intelligenza artificiale generativa, ma il loro output (il testo tradotto) è in ogni caso una diretta trasformazione dell'input (il testo da tradurre), e quindi probabilmente non da considerarsi in modo speciale.  
Ogni pagina tradotta automaticamente presenta un avviso all'inizio.

## Immagini

Sono stati usati modelli generativi di immagini per creare copertine per alcuni post, a scopo unicamente decorativo ma che richiami il contenuto, in tutti quei casi in cui non sia stato possibile procurarsi o creare un'immagine con il giusto feeling attraverso metodi classici.  
Per i post che fanno parte di questo gruppo, di seguito riportati, è in ogni caso indicato in un punto leggibile della pagina, in genere nelle note, la natura delle immagini di copertina utilizzate:

* [{{< relref "/blog/2023-08-11-Recensione-Pipi-Nel-Mar-Tirreno/" >}}]({{< relref "/blog/2023-08-11-Recensione-Pipi-Nel-Mar-Tirreno/" >}})
* [{{< relref "/blog/2023-09-15-Mollette-per-Capelli-Colla-e-Sassolini/" >}}]({{< relref "/blog/2023-09-15-Mollette-per-Capelli-Colla-e-Sassolini/" >}})
* [{{< relref "/blog/2024-06-22-Diari-Blog-Social-Riflessione/" >}}]({{< relref "/blog/2024-06-22-Diari-Blog-Social-Riflessione/" >}})
* [{{< relref "/note/2024-09-01-Compilare-Installare-Linux-Kernel/" >}}]({{< relref "/note/2024-09-01-Compilare-Installare-Linux-Kernel/" >}})
* [{{< relref "/blog/2024-09-23-Anti-Woke-Gaming/" >}}]({{< relref "/blog/2024-09-23-Anti-Woke-Gaming/" >}})

Considerata la loro natura, queste immagini vengono considerate come tappabuchi temporanei, e possono venir sostituite in futuro con delle versioni definitive non generate con intelligenza artificiale, qualora dei sostituti adeguati diventino disponibili.  
Eccetto quando diversamente specificato, ad esempio nel caso di un'immagine generata usata come base al di sopra della quale creare manualmente un'immagine derivativa sufficientemente differente da considerarsi un'opera nuova, non detengo il copyright delle immagini generate tramite intelligenza artificiale generativa, e quindi il loro riutilizzo in qualsiasi forma e per qualsiasi scopo deve essere valutato individualmente dai singoli utenti in base alle leggi morali e dello Stato.[^copyright]  
I prompt utilizzati per generare le immagini sono relativamente generici e non tentano di copiare né lo stile di artisti esistenti o esistiti, né il contenuto di specifiche opere dell'ingegno umano esistenti o esistite.  
Inoltre ricordo che l'intero sito non è a scopo di lucro.

## {{% i18n notes-refs %}}

[^concerns]: <https://en.wikipedia.org/wiki/Generative_artificial_intelligence#Concerns>
[^copyright]: <https://en.wikipedia.org/wiki/Artificial_intelligence_and_copyright>
