+++
Title = "⚜️ Raccolta Emblemi"
Categories = [ "Fritto-Misto" ]
Lastmod = 2024-12-08
+++

Piastrellati sul sitoctt, così come sui siti di altre persone _che vivono nel mio computer_, ci sono queste immaginette colorate. A volte animate, a volte con qualcosa di sensato scritto dentro ed altre no, ed alcune che linkano ad altre pagine o comunque fanno qualcosa di utile.

Ce ne sono di diversi tipi, con diversi nomi: bottoni o banner (di solito grandi 88x31 pixel), _blinkie_ (più bassi e larghi, di solito 150x20 e animati), o vattelappesca. Alla fine sono in qualche modo tutti assimilabili a degli emblemi, e quindi ho creato questa pagina per raccoglierli; elencando innanzitutto quelli che uso in generale nel sito, aggiungendo maggiori informazioni, e magari piazzandone altri che mi piacciono trovati in giro.

_In costruzione..._

## Quelli usati qui

{{< footer-buttons extended=true >}}

## Altre Risorse

Ovviamente, ci sono sia siti che raccolgono e catalogano quantità industriali di queste immagini, sia paginette come questa su altri siti personali. Ecco quindi altre raccolte che conosco e consiglio (chi ne conosce altre può lasciarle nei commenti a questa pagina):

* (11K+) _The 88x31 GIF Banner Database_: https://88x31db.com
* (4K+) _The 88x31 GIF Collection_: https://cyber.dabamos.de/88x31/
   * Mirror: https://www.deadnet.se/88x31/
* ![]({{< assetsRoot >}}/Media/Buttons/88x31/Sites/hellnet_8831.gif) (31K+) _THE 88×31 ARCHIVE_: https://hellnet.work/8831/
* _88x31 Button Library_: https://lazerdart.uk/buttons
* (27K+) _The 88x31 GIF Collection_: http://www.textfiles.com/underconstruction/88x31/ (attenzione: pagina pesantissima)
* _kate's 88x31 button archive_: https://88x31.kate.pet — https://github.com/ktwrd/88x31
* ![]({{< assetsRoot >}}/Media/Buttons/88x31/Sites/the-largest-88x31-collection-2.png) _the largest 88x31 collection on the internet_: https://capstasher.neocities.org/88x31collection-page1
* _A.N. Lucas's 88x31 button Collection_: https://anlucas.neocities.org/88x31Buttons — https://news.ycombinator.com/item?id=33793273
* ![]({{< assetsRoot >}}/Media/Buttons/88x31/Sites/winter-self.png) https://winter.entities.org.uk/buttons/
* ![]({{< assetsRoot >}}/Media/Buttons/88x31/Sites/fustilugz-ME%20BUT%20SMALLer.gif) _Fustilugz's Button Collection_: https://fustilugz.neocities.org
* _My 88x31 Button Store_: https://88x31.nekoweb.org
* ![]({{< assetsRoot >}}/Media/Buttons/88x31/Sites/dannarchy-d_now.gif) _88x31 (Now) Buttons_: https://dannarchy.com/now
* ![]({{< assetsRoot >}}/Media/Buttons/88x31/Sites/nekobuttons.png) _Neko Buttons_: https://buttons.nekoweb.org
* ![]({{< assetsRoot >}}/Media/Buttons/88x31/Sites/dokodemobutton3.gif) _DOKODEMO Buttons_: https://dokode.moe/buttons

Ci sono anche strumenti di creazione e simili risorse, a proposito:

* _blinkie maker | generate blinkie gifs with custom text!_: https://blinkies.cafe — https://github.com/piconaut/blinkies.cafe — offre vari template e permette di usarli per creare blinkie con il testo che si vuole
* _88x31 Web Button Maker_: https://hekate2.github.io/buttonmaker/ — https://github.com/hekate2/buttonmaker
* _88x31 Button Maker_: https://sadgrlonline.github.io/88x31-button-maker/ — https://github.com/sadgrlonline/88x31-button-maker

Ancora altre informazioni e risorse sull'argomento:

* ![]({{< assetsRoot >}}/Media/Buttons/88x31/Sites/bmhonline_button2.png) _How2 - 88x31 Buttons | BMH Online_: https://bmh.neocities.org/how2/buttons — spiega ancora cose sui bottoni 88x31
* <https://eightyeightthirty.one> — https://breq.dev/projects/eightyeightthirtyone — servizio che fa crawling di tutti i possibili siti seguendo i link dei bottoni 88x31, mostrando un grafico a rami

Consiglio: Gli emblemi usati sul mio sito sono tutti ri-hostati da me, e nel processo di ricaricarli mi assicuro di comprimerli con i migliori algoritmi lossless in circolazione, qualora sia possibile (ad esempio, per i file PNG), così che occupino il minor spazio possibile pur senza perdere alcun grado di qualità dell'immagine. Consiglio a tutti gli altri di adoperarsi per fare lo stesso, ci sono solo vantaggi.

