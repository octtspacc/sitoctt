+++
Title = "Progetti⚒️"
Lastmod = 2025-02-15
+++

Questa sezione raccoglie (quasi) tutti i miei progetti (di cui uno è il sitoctt stesso). Per informazioni aggiuntive, leggi il post di presentazione: <https://octospacc.altervista.org/2025/02/14/definitivo-progettocto-di-progettazioctt/>.
