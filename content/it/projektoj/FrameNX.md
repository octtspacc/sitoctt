+++
Title = "FrameNX"
Slug = "FrameNX"
canonicalUrl = "https://octt.itch.io/framenx"
+++

Tutte le informazioni, i pacchetti eseguibili e il codice sorgente sono disponibili su itch.io:

* https://octt.itch.io/framenx

## Pagina originale

<blockquote>

<!-- {{< GetRemote "https://octt.itch.io/framenx" html split `<div class="formatted_description user_formatted">` 1 `</div>` 0 >}} -->

{{< GetRemote "https://octt.itch.io/framenx" html split `<div class="right_col column">` 1 `</div></div></div><div id="view_game_footer" class="footer">` 0 >}}

{{< GetRemote "https://octt.itch.io/framenx" html split `<div class="left_col column">` 1 `</div><div class="right_col column">` 0 >}}

</blockquote>
