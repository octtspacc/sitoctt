+++
Title = "Spaccogram (Web)"
Slug = "Spaccogram"
+++

{{< embed "https://1.tgweb.octt.eu.org/" window >}}

Tutte le informazioni sono disponibili sul canale Telegram:

* https://t.me/+ujaob63Vy705Mzgx

Il codice sorgente è disponibile su Git:

* https://gitea.it/octospacc/Spaccogram
