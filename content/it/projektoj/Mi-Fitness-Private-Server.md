+++
Title = "Mi Fitness Private Server"
Slug = "Mi-Fitness-Private-Server"
canonicalUrl = "https://github.com/octospacc/Mi-Fitness-Private-Server"
+++

**Mi Fitness Private Server** è un esperimento di reverse-engineering per reimplementare da zero un server API e CDN completamente indipendente per la app _Mi Fitness_ di Xiaomi, per sostituire quello ufficiale, in modo da sbloccare più funzionalità e, in futuro, garantire il funzionamento della app in completa privacy e senza dipendenza dal server ufficiale.

Tutte le informazioni e il codice sorgente sono disponibili su Git:

* https://gitlab.com/octospacc/Mi-Fitness-Private-Server
* https://github.com/octospacc/Mi-Fitness-Private-Server
* https://gitea.it/octospacc/Mi-Fitness-Private-Server

## LEGGIMI originale

<blockquote>

{{< GetRemote "https://gitlab.com/octospacc/Mi-Fitness-Private-Server/-/raw/main/README.md" markdown >}}

</blockquote>
