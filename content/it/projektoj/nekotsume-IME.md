+++
Title = "nekotsume IME"
Slug = "nekotsume-IME"
canonicalUrl = "https://github.com/octospacc/nekotsume-ime"
+++

Tutte le informazioni e il codice sorgente sono disponibili su Git:

* https://gitlab.com/octospacc/nekotsume-ime
* https://github.com/octospacc/nekotsume-ime
* https://gitea.it/octospacc/nekotsume-ime

## LEGGIMI originale

<blockquote>

{{< GetRemote "https://gitlab.com/octospacc/nekotsume-ime/-/raw/main/README.md" markdown >}}

</blockquote>
