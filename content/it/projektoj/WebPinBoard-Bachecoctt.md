+++
Title = "WebPinBoard / Bachecoctt"
Slug = "WebPinBoard-Bachecoctt"
canonicalUrl = "https://github.com/octospacc/WebPinBoard"
+++

{{< embed "https://octtspacc.gitlab.io/bachecoctt/" window >}}

Tutte le informazioni e il codice sorgente sono disponibili su Git:

* https://gitlab.com/octospacc/WebPinBoard
* https://github.com/octospacc/WebPinBoard
* https://gitea.it/octospacc/WebPinBoard
* https://codeberg.org/octt-mirror/WebPinBoard

## LEGGIMI originale

<blockquote>

{{< GetRemote "https://gitlab.com/octospacc/WebPinBoard/-/raw/main/README.md" markdown >}}

</blockquote>
