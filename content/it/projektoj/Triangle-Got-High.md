+++
Title = "Triangle Got High"
Slug = "Triangle-Got-High"
canonicalUrl = "https://octt.itch.io/triangle-got-high"
+++

Tutte le informazioni, i pacchetti eseguibili e il codice sorgente sono disponibili su itch.io:

* https://octt.itch.io/triangle-got-high

## Pagina originale

<blockquote>

{{< GetRemote "https://octt.itch.io/triangle-got-high" html split `<div class="right_col column">` 1 `</div></div></div><div id="view_game_footer" class="footer">` 0 >}}

{{< GetRemote "https://octt.itch.io/triangle-got-high" html split `<div class="left_col column">` 1 `</div><div class="right_col column">` 0 >}}

</blockquote>
