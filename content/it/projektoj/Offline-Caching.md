+++
Title = "Offline Caching"
Slug = "Offline-Caching"
canonicalUrl = "https://addons.mozilla.org/firefox/addon/offline-caching/"
+++

Tutte le informazioni e il codice sorgente sono disponibili su:

* https://addons.mozilla.org/firefox/addon/offline-caching/

## LEGGIMI originale

<blockquote>

{{< GetRemote "https://addons.mozilla.org/firefox/addon/offline-caching/" html split `<div class="Addon-main-content">` 1 `</div><section ` 0 >}}

</blockquote>
